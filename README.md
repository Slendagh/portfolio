## Setup

### Initial installation

To setup the project:
1. git pull the project
2. copy .env_example to .env and fill the variables
3. run `docker-compose up`
4. in the container, run `composer update` to install dependencies


### Enter php container
To run composer, artisan and npm, enter the php container using this command:
`docker-compose exec web bash`

To be run in the project's root.


### Dev environment
1. Enter the web container with `docker-compose exec web bash`
2. Run `npm run watch`