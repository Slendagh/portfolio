@extends('errors::minimal')

@section('title', __('Service Unavailable'))
@section('message', __('This page doesn\'t exists.'))