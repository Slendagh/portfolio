@extends('errors::minimal')

@section('title', __('Service Unavailable'))
@section('message', __('Oh, a problem has occurred !'))