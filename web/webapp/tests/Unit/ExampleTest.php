<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\TagParser;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParsesSingleTag()
    {
        $parser = new TagParser;
        $result = $parser->parse('personal');
        $expected = ['personal'];

        $this->assertSame($expected, $result);
    }

    public function testParsesMultipleTag()
    {
        $parser = new TagParser;
        $result = $parser->parse('personal, money, family');
        $expected = ['personal', 'money', 'family'];

        $this->assertSame($expected, $result);
    }
}
