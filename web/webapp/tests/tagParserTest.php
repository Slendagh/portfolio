<?php

namespace Tests;
    
class tagParserTest
{
    public function test_parses()
    {     
            $parser = new Example;
            $result = $parser->parse('personal, money, family');
            $expected = ['sonal', 'money', 'family'];

            $this->assertSame($expected, $result);
    }

}
   