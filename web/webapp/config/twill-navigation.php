<?php

return [
    'articles' => [
        'title' => 'Articles',
        'module' => true
    ],
    'projects' => [
        'title' => 'Projects',
        'module' => true
    ],
    'galleries' => [
        'title' => 'Galleries',
        'module' => true
    ]
];
