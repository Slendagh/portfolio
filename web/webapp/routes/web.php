<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('welcome')->get('/', 'HomeController@index');

Route::name('articles.index')->get('blog', 'ArticleController@index');
Route::name('articles.show')->get('blog/{slug}', 'ArticleController@show');

Route::name('portfolio.index')->get('portfolio', 'ProjectController@index');
Route::name('gallery.index')->get('gallery', 'GalleryController@index');

Route::get('blog?tag={name}', 'ArticleController@index');

Route::name('about')->get('about', function(){
    return view('about');
});

Route::post('contactme/send', 'HomeController@sendMail');


