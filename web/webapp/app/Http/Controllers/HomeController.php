<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ArticleRepository;
use App\Models\Article;
use App\Models\Gallery;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    public function index(){
        $article = Article::published()->inRandomOrder()->first();
        return view('welcome', [
            'article' => $article
        ]);
        
    }

    public function sendMail(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);
        $email = $request->email;
        $name = $request->name;
  
        return view('welcome', ['success' => 'Your message has been sent successfully']);
    }
}
