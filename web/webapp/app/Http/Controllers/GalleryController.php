<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GalleryRepository;


class GalleryController extends Controller
{
    //
    public function __construct(GalleryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('gallery.index', [
            'images' => $this->repository->allImages()
        ]);
    }
}
