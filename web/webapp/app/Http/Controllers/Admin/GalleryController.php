<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class GalleryController extends ModuleController
{
    protected $moduleName = 'galleries';
}
