<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ArticleRepository;
use App\Models\Article;
use App\Models\Tag;

class ArticleController extends Controller
{
    public function __construct(ArticleRepository $repository)
    {
        $this->repository = $repository;
    }
   

    public function index(Request $request){
        $array = $this->repository->articlesIndex($request->query('tag'));
        $tags = \App\Tag::where('namespace', 'App\Models\Article')->pluck('slug');
        return view('articles.index', [
            'articles' => $array[0],
            'latest' => $array[1],
            'tags' => $tags
        ]);
        
    }

    public function show($slug)
    {
        $article = $this->repository->forSlug($slug);
        abort_unless($article, 404, 'Article');

        return view('articles.show', compact('article'));
    }   
}
