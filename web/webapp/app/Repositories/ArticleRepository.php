<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\Behaviors\HandleTags;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Article;

class ArticleRepository extends ModuleRepository
{
    use HandleSlugs, HandleMedias, HandleFiles, HandleRevisions, HandleTags;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function articlesIndex($tag) 
    {
        /*return $this->model 
            ->published()
            ->orderBy('publish_start_date')
            ->get();*/
        if($tag == true){
            $allArticles = collect($this->model->withTag([$tag])->published()->orderBy('publish_start_date', 'DESC')->get());
            $latest = $allArticles->shift();
            return [$allArticles, $latest];
        }else{
            $allArticles = collect($this->model->published()->orderBy('publish_start_date', 'DESC')->get());
            $latest = $allArticles->shift();
            return [$allArticles, $latest];
        }
        
    }

}
