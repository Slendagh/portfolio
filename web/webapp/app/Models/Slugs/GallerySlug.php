<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GallerySlug extends Model
{
    protected $table = "gallery_slugs";
}
