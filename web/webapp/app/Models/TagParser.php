<?php 
namespace App\Models;

class TagParser
{
    public function parse(string $tags)
    {
        return explode(', ', $tags);
    }
}