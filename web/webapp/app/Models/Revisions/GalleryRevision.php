<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GalleryRevision extends Revision
{
    protected $table = "gallery_revisions";
}
