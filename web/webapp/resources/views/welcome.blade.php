@extends('layouts.layout')

@section('content')

<section id="intro" class="d-flex flex-column">
    <div class="hello">Hello, my name is</div>
    <div><h1>Asmaa<br/>Azaroual</h1></div>
    <div>
        <!--<h4>A junior <strong>backend developer</strong><br/>-->
        I create <strong>web sites & web applications</strong></h4>
    </div>
    <div class="d-flex flex-row align-items-baseline" style="padding-top: 20px;">
        <div class="border-round">
            <div class="round"></div>
        </div>
        <div style="font-size: 0.9rem;font-family: 'Roboto Slab', serif;">&nbsp;I'm currently accepting work</div><div>&nbsp;&nbsp;<a href="mailto: azaroual.asmaa@gmail.com" id="contactme">Contact me</a></div>
    </div>
</section>

@endsection

            
        
