<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56051904-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-56051904-1');
        </script>

        <!--meta-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Asmaa Azaroual's portfolio">
        <meta name="keywords" content="web dev, laravel, portfolio, php">
        <meta name="author" content="Asmaa Azaroual">

        <title>Asmaa Azaroual</title>

        <!--Style sheets-->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('lightbox2-2.11.3/dist/css/lightbox.min.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&family=Roboto+Slab:wght@100;400&display=swap" rel="stylesheet">
        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{asset('lightbox2-2.11.3/dist/js/lightbox-plus-jquery.min.js')}}"></script>
        
    </head>
    <body class="d-flex flex-column justify-content-between">

        <!--nav bar here-->
        <nav class="nav-width navbar navbar-expand-lg nav-class navbar-light" id="navbar">
            <a class="navbar-brand {{ Route::is('welcome') ? 'notvisible' : 'visible' }}"  href="{{ url('/') }}">Asmaa<br/>Azaroual</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto mr-5">
                    <li class="nav-item mr-2">
                        <a class="nav-link {{ Route::is('portfolio.index') ? 'active' : '' }}" href="{{url('/portfolio')}}">Portoflio</a>
                    </li>
                    <li class="nav-item mr-2">
                        <a class="nav-link {{ Route::is('articles.index') ? 'active' : '' }} {{ Route::is('articles.show') ? 'active' : '' }}" href="{{url('/blog')}}">Blog</a>
                    </li>
                    <li class="nav-item mr-2">
                        <a class="nav-link {{ Route::is('about') ? 'active' : '' }} {{ Route::is('about') ? 'active' : '' }}" href="{{url('/about')}}">About me</a>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('gallery.index') ? 'active' : '' }}" href="{{url('/gallery')}}">Gallery</a>
                    </li>
                    -->
                </ul>
            </div>
        </nav>
        <!--end of nav bar-->

            @yield('content')

        <!--</div>-->
        <footer id="contact" style="background-color:#101726; height:130px;padding-bottom: 2rem;">
            <h3 class="text-center mt-3 text-light mb-4">Let's Connect</h3>
            <div class="d-flex flex-row justify-content-center">
                <a href="https://twitter.com/Slendagh" class="mr-3 text-light"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <!--<a href="https://youtube.com" class="mr-3 text-light"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>-->
                <a href="https://www.linkedin.com/in/asmaa-azaroual-272b93149/" class="mr-3 text-light"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
        </footer>

    </body>
</html>