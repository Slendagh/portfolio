<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Asmaa Azaroual</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{asset('lightbox2-2.11.3/dist/js/lightbox-plus-jquery.min.js')}}"></script>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('lightbox2-2.11.3/dist/css/lightbox.min.css') }}">
        
        <!-- Styles -->
        <style>
            html, body {
                background-color: #343a40;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                /*font-weight: 200;*/
                height: 100vh;
                margin: 0;
                font-size:16px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <!--<div class="flex-center position-ref full-height">-->
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <!--nav bar here-->
            <nav class="navbar navbar-expand-lg" id="navbar">
                <a class="navbar-brand text-white" href="{{ url('/') }}">Asmaa Azaroual Logo</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto mr-5">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('/portfolio')}}">Portoflio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('/articles')}}">Blogging</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="{{url('/gallery')}}">Gallery</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!--end of nav bar-->

            @yield('content')

        <!--</div>-->
        <section class="mt-5 p-2 footer">
            <h6 class="text-center">This website is made with Twill</h6>
        </section>
        
    </body>
</html>