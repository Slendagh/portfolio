@extends('layouts.layout')

@section('content')

<style>
    #someDiv {
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    }
</style>

<!--this section bellow was commented befooore-->
@if(!is_null($latest) && !is_null($articles))
<!--<button type="button" class="btn btn-light">{{$latest->tags[0]->name}}</button>
@foreach($articles as $article)
    @foreach($article->tags as $tag)
    <button type="button" class="btn btn-light">{{$tag->name}}</button>
    @endforeach
@endforeach-->

<!--
<section class="tags mt-5">
    <div class="d-flex justify-content-center">
        @foreach($tags as $tag)
        <a href="blog?tag={{$tag}}" class="mr-3 btn btn-light"># {{$tag}}</a>
        @endforeach
        <a href="blog" class="mr-3 btn btn-light"># all</a>
    </div>
</section>

<section id="desktop_latest" class="latest mt-5">
    <div class="container mt-5">
        <div class="row">
            <div class="card m-auto" style="width: 66rem; height:inherit;">
                <a href="/blog/{{$latest->slug}}">
                    <div class="d-flex flex-row">
                        <img src="{{$latest->image('screengrab_desktop', 'default', ['w'=>300, 'fit'=>null])}}" class="card-img-top w-25 h-100" alt="...">
                        <div class="card-body h-100 justify-content-center">
                            <h5 class="card-title">{{$latest->title}}</h5>
                            <p class="card-text">{{\Illuminate\Support\Str::limit(strip_tags($latest->text), $limit=150, $end='...')}}</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>-->

<!--for mobile
<div id="mobile_latest" class="container mt-5">
    <div class="row m-auto">
        <div class="card m-auto" style=" width:40rem; height:inherit;">
            <a href="/blog/{{$latest->slug}}">
                <div class="d-flex flex-column">
                    <img src="{{$latest->image('screengrab_phone', 'default', ['w'=>300, 'fit'=>null])}}" class="card-img-top" alt="...">
                    <div class="card-body h-100 justify-content-center">
                        <h5 class="card-title">{{$latest->title}}</h5>
                        <p class="card-text">bl;a bla ba hfhegfulv kjlegbgr khrblie</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>-->
<!--end mobile version

<div class="container mt-5">
    <div class="row">
    @foreach($articles as $article)
        <div class="col-sm">
            <div class="card mb-5" style="width: 20rem;">
                <a href="/blog/{{$article->slug}}">
                    <img src="{{$article->image('screengrab_desktop', 'default', ['w'=>300, 'fit'=>null])}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$article->title}}</h5>
                        <p class="card-text"></p>
                    </div>
                </a>
            </div>
        </div>
    @endforeach
    </div>
</div>
-->@endif
<div class="mb-8 d-flex flex-column justify-content-center align-items-center" style="height:100vh;">
    <img src="{{ URL::to('/my_images/tea.gif') }}">
    <h2>COMING SOON</h2>
</div>
<!--<script src="https://cdn.lordicon.com//libs/frhvbuzj/lord-icon-2.0.2.js"></script>-->

@endsection