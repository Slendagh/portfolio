@extends('layouts.layout')

@section('content')

<!--<div class="d-flex justify-content-center">
@foreach($article->tags as $tag)
    <button type="button" class="btn btn-light">{{$tag->name}}</button>
@endforeach
</div>-->
<style>
    .ql-syntax{
        background-color:black;
        color:white;
        padding:20px;
        border-radius:5px;
    }
    .subheading{
        display: block;
        font-size: 75%;
        opacity: 0.75;
    }
</style>

<section class="article">
    <div class="mt-5 p-2 d-flex flex-column">
        <div class="ml-3">
            <div class="mb-5 mt-3 text-center">
                <h1 class="">
                    {!! $article->title !!}
                </h1>
                <span class="subheading">Published on {{ Carbon\Carbon::parse($article->publish_start_date)->format('d F Y')}} . 4min read</span>
            </div>
            <div class="m-auto" style="width:100%; max-width:900px;">
                {!! $article->text !!}
            </div>
        </div>
    </div>
</secion>

@endsection