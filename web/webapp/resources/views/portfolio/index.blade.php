@extends('layouts.layout')

@section('content')

<section id="projects">
    @foreach($projects as $project)
    <div class="card">
        <?php $images_screen = $project->images('screen', 'default'); $i=0;?>
        @if(count($images_screen) > 1)
        <div id="carouselId{{$project->id}}" class="carousel slide main-img" data-ride="carousel" data-interval="false">
            <ol class="carousel-indicators">
                <?php for($k=0; $k<count($images_screen); $k++){ ?>
                    @if($k == 0)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$k}}" class="active"></li>
                    @else
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$k}}"></li>
                    @endif
                <?php } ?>
            </ol>
            <div class="carousel-inner">
                @foreach($images_screen as $img_screen)
                    <?php ++$i; ?>
                @if($i == 1)
                <div class="carousel-item active">
                @else
                <div class="carousel-item">
                @endif
                    <img class="d-block w-100" src="{{$img_screen}}">
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselId{{$project->id}}" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselId{{$project->id}}" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        @elseif(count($images_screen) == 1)
        <img class="main-img" src="{{$project->image('screen', 'default')}}"  alt="..."/>
        @endif
        <div class="text">
            <strong>{{$project->title}}</strong>
            <p>{{$project->description}}</p>
            <?php
                $images = $project->images('logos', 'default');
            ?>
            <div class="logos">
            @foreach($images as $image)
                <img src="{{$image}}">
            @endforeach
            </div>
            <?php
                if(strpos($project->url, 'gitlab') !== false){ 
            ?>
                    <a href="{{$project->url}}" class="btn w-full seeCode">See Code</a>
            <?php
                }else{
            ?>
                    <a href="{{$project->url}}" class="btn w-full seeCode" style="display:none;">Visit site</a>
            <?php } ?>
        </div>
    </div>
    @endforeach
</section>

@endsection