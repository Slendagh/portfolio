@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'maxlength' => 200,
        'required' => true,
    ])

    @formField('wysiwyg', [
        'name' => 'text',
        'label' => 'Article',
        'toolbarOptions' => [
            ['header' => [2, 3, 4, 5, 6, false]],
            'bold',
            'italic',
            'underline',
            'strike',
            ['color' => []],
            ["script" => "super"],
            ["script" => "sub"],
            "blockquote",
            "code-block",
            ['list' => 'ordered'],
            ['list' => 'bullet'],
            ['indent' => '-1'],
            ['indent' => '+1'],
            ["align" => []],
            ["direction" => "rtl"],
            'link',
            'image',
            "clean",
        ],
        'placeholder' => 'Article',
        'maxlength' => 8000,
        'note' => 'Hint message',
        'rows' =>10
    ])

    
    @formField('date_picker', [
    'name' => 'publish_start_date',
    'label' => 'Publish date',
    'minDate' => '2021-01-01 12:00',
    'maxDate' => '2025-12-10 12:00'
    ])
    
    @formField('tags')

    @formField('medias', [
        'name' => 'screengrab_desktop',
        'label' => 'Desktop',
        'note' => 'Shown at a desktop breakpoint.'
    ])

    @formField('medias', [
        'name' => 'screengrab_tablet',
        'label' => 'Tablet',
        'note' => 'Shown at a tablet breakpoint.'
    ])

    @formField('medias', [
        'name' => 'screengrab_phone',
        'label' => 'Phone',
        'note' => 'Shown at a phone breakpoint.'
    ])
@stop
