@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'url',
        'label' => 'Gitlab URL',
        'placeholder' => 'http://www.gitlab.com/project',
        'note' => 'Please enter full URL',
        'maxlength' => 200
    ])

    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'maxlength' => 200
    ])

    @formField('medias', [
        'name' => 'logos',
        'label' => 'Logos',
        'note' => 'Shown at a desktop breakpoint.',
        'max' =>4,
    ])

    @formField('medias', [
        'name' => 'screen',
        'label' => 'Screen',
        'note' => 'Shown at a desktop breakpoint.',
        'max' =>3,
    ])

    @formField('medias', [
        'name' => 'gif',
        'label' => 'Gif',
        'note' => 'Shown at a desktop breakpoint.',
    ])





@stop
