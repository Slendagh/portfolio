@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'maxlength' => 100
    ])

    @formField('input', [
        'name' => 'location',
        'label' => 'Location',
        'maxlength' => 100
    ])

    @formField('medias', [
    'name' => 'image',
    'label' => 'Image',
    'note' => 'Also used in listings',
    'fieldNote' => 'Minimum image width: 1500px'
]) 
@stop
