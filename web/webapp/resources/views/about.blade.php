@extends('layouts.layout')

@section('content')

<section id="aboutme">
    <div class="section-title">About me</div>
    <div style="width: 100%;text-align: center;max-width: 500px;padding: 10px;">
        I'm a junior full stack developer, with intermediate skills in back end developement using Laravel and postgresql as my main tools.
        <br><br>
        My interest in programming started in 2013, it was then when I started learning HTML5, CSS3 and some Javascript. I was a medical student at the time which meant I didn't have enough time or energy to keep up with my self-learning journey.
        <br><br>
        In 2020 I got a diploma in applied computer science where I learnt the fundamental layers of programming concepts using different coding languages.
    </div>
    <div class="conclusion">
        <div class="title-conclusion"><strong>Today, I'm working as a freelancer, building websites and web applications.</strong></div>
        <div class="body-conclusion">check out my portfolio to see some of my personal projects or go to my blog page to read about the things I'm learning.</div>
        <div class="conclusion-buttons">
            <a href="{{ url('/portfolio')}}" class="knowmore btn">My portfolio</a>
            <a href="{{ url('/blog')}}" class="knowmore btn">My blog</a>
        </div>
    </div>
</section>

@endsection